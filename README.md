# GNU BEAUTIFUL CHOOSER v0.01 alpha #

A csvtool helper for choosing a random subset of entries from a multiple choice .csv question database with the format: 

> "COL1: Question categories","COL2: Question","COL3: Answers","COL4: Correct answer"

It currently exports two .docx files

Version 0.01 is almost pseudo-code translated into the shell.

---

Copyright (C) 2017, Jose Serralde <contacto@joseserralde.org>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.  

You should have received a copy of the GNU Lesser General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

---

## Dependencies

- [pandoc](http://pandoc.org/)
- [csvtool](https://sourceforge.net/projects/cvstool/)

## To-do's

- Add better cmd-line parsing with flags and options.
- Clean bash code.
- Attain POSIX & l10n conventions. 
- Add zenity UI.
- Make pandoc output optional configurable.
