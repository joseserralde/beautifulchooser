#!/bin/bash
#
# GNU BEAUTIFUL CHOOSER v0.01
#
# A csvtool helper for choosing a random subset of entries/questions, from a
# multiple choice .csv database with the format:
# "COL1: Question categories","COL2: Question","COL3: Answers","COL4: Correct answer"
# Version 0.01 is almost pseudo-code translated into the shell.
#
# Copyright (C) 2017, Jose Serralde <contacto@joseserralde.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


# Configuration functions

function config {


	# Defaults

	TEMPDIR="/tmp"
	DESTDIR="./"

	# Hardcoded variables

	DEPENDENCIES="pandoc csvtool"

	# UI Variables

	CMD_PRINT_MESSAGE="echo"

	config_l10n

	$CMD_PRINT_MESSAGE "$MSG_WELCOME" # Welcome

	config_checkdep


}

function config_checkdep { # Check for needed dependencies

	for i in $DEPENDENCIES
	do
		! test -x /usr/bin/$i \
			&& quit_depend
	done

}

function config_l10n {

	MSG_WELCOME="GNU BEAUTIFUL CHOOSER v0.01 by José Serralde <contacto@joseserralde.org>"
	MSG_RANDOMPOOL="Creating the random pool in file: "
	MSG_GRAB_QUESTION="Grabbing question: "
	MSG_GENERATING_PANDOC="Creating DOCX files (PANDOC)"
	MSG_CLEANING="Cleaning..."
	MSG_KEY="**Key/Clave**"
	MSG_SUCCESS="DONE."
	MSG_ERROR_DEPEND="ERROR: I cannot find $i the system path.Try: sudo apt-get install $i maybe."
	MSG_ERROR_NOFILE="ERROR: No file provided"
	MSG_USAGE="Usage: bch.sh <filename.csv> [number-of-questions-to-be-chosen]"
	MSG_WARN_NONUMBER="WARNING: NO NUMBER of questions specified. Using all."
	MSG_NUMBER_OF_SELECTED="Number of questions to be chosen: "

}

# Task functions


function quit_depend {
	$CMD_PRINT_MESSAGE "$MSG_ERROR_DEPEND"
	exit
}

function quit_nofile {
	$CMD_PRINT_MESSAGE "$MSG_ERROR_NOFILE"
	$CMD_PRINT_MESSAGE "$MSG_USAGE"
	exit
}


function randompool {

	get_total # get NUMBER_OF_QUESTIONS
	get_randomfilename # get SHA256NAME

	if [ -z $NUMBER_OF_SELECTED ]
	then
		NUMBER_OF_SELECTED=$NUMBER_OF_QUESTIONS
	fi

	$CMD_PRINT_MESSAGE  "$MSG_RANDOMPOOL $SHA256_FILENAME"

	echo '' > $SHA256_FILENAME

	for i in $(seq $NUMBER_OF_QUESTIONS)
	do
		echo $i
	done | shuf | head -$NUMBER_OF_SELECTED > $SHA256_FILENAME

}

function get_total {
	NUMBER_OF_QUESTIONS=$(csvtool height "$CSVFILE")
}

function get_randomfilename {
	SHA256_FILENAME=$(date | sha256sum | head -c 8)
}

function parser {
	
	MARKDOWN_FILE="${SHA256_FILENAME%%.txt}.md"
	MARKDOWN_KEYFILE="${SHA256_FILENAME%%.txt}-key.md"
	DOCX_FILE="${SHA256_FILENAME%%.txt}.docx"
	DOCX_KEYFILE="${SHA256_FILENAME%%.txt}-key.docx"



	## Create markdown document from POOL

	QUESTION_NUMBER=$(( 1 ))
  echo "$MSG_KEY: $SHA256_FILENAME, $NUMBER_OF_SELECTED/$NUMBER_OF_QUESTIONS" \
		> $MARKDOWN_FILE # CLEAN MARKDOWN TEMPORARY FILE
	echo >> $MARKDOWN_FILE

  echo "$MSG_KEY: $SHA256_FILENAME, $NUMBER_OF_SELECTED/$NUMBER_OF_QUESTIONS" \
		> $MARKDOWN_KEYFILE # CLEAN MARKDOWN TEMPORARY FILE
	echo >> $MARKDOWN_KEYFILE

	for i in $(cat $SHA256_FILENAME)
	do
		FORMAT="$QUESTION_NUMBER. %(2)\n\n%(3)\n\n"
		FORMAT_FOR_KEY="$QUESTION_NUMBER. %(4)\n"

	 	# For debugging uncomment:
	  # $CMD_PRINT_MESSAGE "$MSG_GRAB_QUESTION" $i

		## Add selected question to exam file

		csvtool drop $(( $i - 1 )) $CSVFILE | csvtool take 1 - \
		| csvtool format "$FORMAT"  - \
		>> $MARKDOWN_FILE

		## Add selected question to keyfile

		csvtool drop $(( $i - 1 )) $CSVFILE | csvtool take 1 - \
		| csvtool format "$FORMAT_FOR_KEY"  - \
		>> $MARKDOWN_KEYFILE

		QUESTION_NUMBER=$(( QUESTION_NUMBER+1 ))
	done

}

function creator {
	## Create DOCX files with pandoc

	$CMD_PRINT_MESSAGE "$MSG_GENERATING_PANDOC"
	pandoc -f markdown $MARKDOWN_FILE -o "$DESTDIR/$DOCX_FILE" 
	pandoc -f markdown $MARKDOWN_KEYFILE -o "$DESTDIR/$DOCX_KEYFILE" 
}

function cleaner { ## Clean temp files
	$CMD_PRINT_MESSAGE "$MSG_CLEANING"
	rm $SHA256_FILENAME
}


## Start

config

if test -z "$1"
then
	echo "$MSG_ERROR_NOFILE"
	quit_nofile
else
	CSVFILE="$1"
	echo "-- $CSVFILE --"
fi

if test -z "$2"
then
	echo "$MSG_WARN_NONUMBER"
else
	NUMBER_OF_SELECTED=$(( $2 ))
	echo "$NUMBER_OF_SELECTED"
fi

randompool
parser && $CMD_PRINT_MESSAGE "$MSG_SUCCESS"
creator && $CMD_PRINT_MESSAGE "$MSG_SUCCESS"
cleaner && $CMD_PRINT_MESSAGE "$MSG_SUCCESS"
